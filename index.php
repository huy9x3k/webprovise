<?php
class Travel
{
    // Enter your code here
}
class Company
{
    // Enter your code here
    public function getValue($object, $key) {
        return isset($object[$key]) ? $object[$key] : null;
    }

    public function calculateTotalCost($list, $costKey)
    {
        return count($list) > 0 ? number_format((float) array_sum(array_map(function ($item) use ($costKey) {
            return (float) $item[$costKey] ? $item[$costKey] : 0;
        }, $list)), 2, '.', '') : 0;
    }

    public function getEmployees($travelData, $companyId)
    {
        return array_values(array_filter($travelData, function ($employee) use ($companyId) {
            return $employee["companyId"] == $companyId;
        }));
    }

    public function getChildCompanies($companies, $parentCompanyId)
    {
        return array_values(array_filter($companies, function ($childCompany) use ($parentCompanyId) {
            return isset($childCompany["parentId"]) && (($childCompany["parentId"] == $parentCompanyId) || ($childCompany["id"] == $parentCompanyId));
        }));
    }
}
class TestScript
{
    public function execute()
    {
        $start = microtime(true);
        // Enter your code here
        $TRAVEL_API = "https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels";
        $COMPANY_API = "https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies";

        $TRAVEL_DATA = json_decode(file($TRAVEL_API)[0], true);
        $COMPANY_DATA = json_decode(file($COMPANY_API)[0], true);

        $result = [];
        
        foreach ($COMPANY_DATA as $index => $company) {
            $companyModel = new Company();
            $innerCompanies = $companyModel->getChildCompanies($COMPANY_DATA, $company["id"]);

            $innerCompaniesWithCost = array_map(function ($innerComp) use ($TRAVEL_DATA, $companyModel) {
                $employees = $companyModel->getEmployees($TRAVEL_DATA, $innerComp["id"]);
                $employeeTotalCost = $companyModel->calculateTotalCost($employees, "price");

                return [
                    "id" => $companyModel->getValue($innerComp, "id"),
                    "name" => $companyModel->getValue($innerComp, "name"),
                    "cost" => $employeeTotalCost,
                    "children" => $employees
                ];
            }, $innerCompanies);

            $result[$index] = [
                "id" => $companyModel->getValue($company, "id"),
                "name" => $companyModel->getValue($company, "name"),
                "cost" => count($innerCompaniesWithCost) > 0
                    ? $companyModel->calculateTotalCost($innerCompaniesWithCost, "cost")
                    : 0,
                "children" => $innerCompaniesWithCost
            ];
        }

        echo json_encode($result);
        echo 'Total time: ' .  (microtime(true) - $start);
    }
}
(new TestScript())->execute();
